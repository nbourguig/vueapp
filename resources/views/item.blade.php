<td class="col-sm-8 col-md-6">
    <div class="media">
        <a class="pull-left" href="#">
            <img class="media-object" src="@{{ item.picture }}" style="width: 72px; height: 72px;">
        </a>

        <div class="media-body">
            <h4 class="media-heading">
                <a href="#">@{{ item.name }}</a>
            </h4>
            <h5 class="media-heading"> by
                <a href="#">@{{ item.brandName  }}</a></h5>
            <span>Status: </span><span v-class="item.statusClass"><strong>@{{ item.status }}</strong></span>
        </div>
    </div>
</td>

<td class="col-sm-1 col-md-1" style="text-align: center">
    <input type="text" class="form-control" v-model="item.quantity">
</td>

<td class="col-sm-1 col-md-1 text-center">
    <strong>$@{{ item.price }}</strong>
</td>

<td class="col-sm-1 col-md-1 text-center">
    <strong>$@{{ item.price * item.quantity }}</strong>
</td>

<td class="col-sm-1 col-md-1">
    <button type="button" class="btn btn-danger"
            v-confirm="{title: 'Remove item', buttonText:'Yes remove it!', callback: removeItem, item: item }">

        <span class="glyphicon glyphicon-remove"></span>
    </button>
</td>