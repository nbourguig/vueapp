<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Shop Cart</title>

    <link rel="stylesheet" href="/assets/vendor/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/vendor/sweetalert/dist/sweetalert.css">
    <link rel="stylesheet" href="/assets/styles/app.css">

    <script src="/assets/vendor/jquery/dist/jquery.min.js"></script>
    <script src="/assets/vendor/lodash/lodash.min.js"></script>
    <script src="/assets/vendor/vue/dist/vue.min.js"></script>
    <script src="/assets/vendor/vue/dist/vue.min.js"></script>
    <script src="/assets/vendor/sweetalert/dist/sweetalert.min.js"></script>
    <script src="/assets/vendor/tequilarapido-app/dist/tequilarapido-app.js"></script>

    <script src="/assets/scripts/vue/directives.js"></script>

    <script src="/assets/scripts/modules/cart.js"></script>
    <script src="/assets/scripts/start.js"></script>

</head>

<body>
    @yield('main')
</body>

</html>