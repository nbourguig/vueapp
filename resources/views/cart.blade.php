@extends('layouts.app')

@section('main')
    <div id="cart-container" class="container">


        <div id="cart-table" class="row"
             lang-product="Product"
             lang-quantity="Quantity"
             lang-price="Price"
             lang-total="Total">

            <h1>@{{ title }}</h1>

            <hr>

            <div class="col-sm-12 col-md-10 col-md-offset-1">

                <table class="table table-hover">
                    <thead>
                        @include('head')
                    </thead>
                    <tbody>


                        <tr v-repeat="item: items">
                            @include('item')
                        </tr>

                        @include('subtotal')
                        @include('shipping')
                        @include('total')
                        @include('actions')
                    </tbody>
                </table>

                <pre>@{{ $data | json }}</pre>
            </div>
        </div>
    </div>
@stop