var sa = function (title, buttonText, callback) {
    swal({
        title: title,
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: buttonText,
        closeOnConfirm: true
    }, function () {
        callback.apply();
    });
};

Vue.directive('confirm', function (args) {
    var el = this.el;

    $(this.el).on('click', function () {
        sa(args.title, args.buttonText, function(){
            args.callback(args.item);
        });
    });
})