(function (app, $) {
    'use strict';

    var module = {}, $ui = {};

    module.container = function () {
        $ui.container = $('#cart-container');
        return $ui.container.length;
    }

    module.init = function () {
        console.log('Init cart module.')
    }

    // Register module
    app.registerModule('cart', module);

})(App, jQuery);


/**
 * VueJs vue
 */
(function (app) {
    app.addModuleVue('cart', 'cartTable', function (module) {
        return new Vue({
            el: '#cart-table',

            props: ['lang-product', 'lang-quantity', 'lang-price', 'lang-total'],

            data: {
                title: 'Shopping cart',
                items: [],
                itemShippingPrice: 0
            },

            computed: {
                canCheckout: function () {
                    for (var i in this.items) {
                        if (this.items[i].quantity < 1) return false;
                    }

                    return this.items.length;
                },

                subtotal: function () {
                    return _.sum(this.items, function (item) {
                        return item.quantity * item.price;
                    });
                },

                estimatedShipping: function () {
                    var nbItems = _.sum(this.items, function (item) {
                        return item.quantity;
                    });

                    return this.itemShippingPrice * nbItems;
                }

            },

            methods: {
                removeItem: function (item) {
                    this.items.$remove(item);
                }
            },

            ready: function () {
                $.get('/cart', function (result) {
                    this.items = result.items;
                    this.itemShippingPrice = result.itemShippingPrice;
                }.bind(this));
            }
        });
    });
})(App);