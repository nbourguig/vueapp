(function (app) {

    'use strict';

    /**
     * Run application modules
     */
    app.module(
        'cart'
    );

    /**
     * Application global onReady
     */
    app.ready(function () {
    });

})(App);